using AzureOrchestrator.Services.ResourceGroup;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateBootstrapLogger();

Log.Information("Starting up AzureOrchestrator");

try
{
    var builder = WebApplication.CreateBuilder(args);

    // serilog config
    builder.Host.UseSerilog((ctx, lc) => lc
        .WriteTo.Console());

    builder.Services.AddControllers();
    builder.Services.AddScoped<IResourceGroupService, ResourceGroupService>();
    builder.Services.AddSwaggerGen();

    var app = builder.Build();
    app.UseSerilogRequestLogging();
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
        options.RoutePrefix = string.Empty;
    });
    app.MapControllers();
    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled exception");
}
finally
{
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}