﻿using System;
using System.Threading.Tasks;
using Azure.Core;
using Azure.Identity;
using Azure.ResourceManager;
using Azure.ResourceManager.Resources;
using Azure.ResourceManager.Resources.Models;

namespace AzureOrchestrator.Services.ResourceGroup

{
    public class ResourceGroupService : IResourceGroupService
    {
        public async Task<string> CreateResourceGroupAsync(CreateResourceGroupDto createResourceGroupDto)
        {
            string tenant = "a11b05df-f04b-4563-8f3f-c84f80060282";
            string clientid = "01dff5e7-1991-41f3-9647-92029859ae32";
            string secret = "i0n8Q~MlwUTtjtdH16l3CxD-kL37y3OKYzBiTb_m";
            string subscriptionid = "91a667f9-8c8e-4a12-b4dc-9da0f2e63e93";
            ClientSecretCredential credential = new ClientSecretCredential(tenant, clientid, secret);
            ArmClient client = new ArmClient(credential,subscriptionid);
            SubscriptionResource subscription = await client.GetDefaultSubscriptionAsync();

            ResourceGroupData resourceGroupData = new ResourceGroupData(AzureLocation.WestEurope);
            ResourceGroupCollection resourceGroups = subscription.GetResourceGroups();
            ArmOperation<ResourceGroupResource> operation = await resourceGroups.CreateOrUpdateAsync(
                Azure.WaitUntil.Completed,
                "newRG",
                resourceGroupData
                );
            ResourceGroupResource newRg = operation.Value;
            return newRg.Data.Name;
        }
    }
}