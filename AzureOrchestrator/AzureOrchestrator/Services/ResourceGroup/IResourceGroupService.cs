﻿namespace AzureOrchestrator.Services.ResourceGroup
{
    public interface IResourceGroupService
    {
        Task<string> CreateResourceGroupAsync(CreateResourceGroupDto createResourceGroupDto);
    }
}