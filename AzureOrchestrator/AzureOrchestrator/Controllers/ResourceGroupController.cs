﻿using AzureOrchestrator.Services.ResourceGroup;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AzureOrchestrator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResourceGroupController : ControllerBase
    {
        private readonly IResourceGroupService _rgService;
        private readonly ILogger<ResourceGroupController> _logger;

        public ResourceGroupController(IResourceGroupService rgService, ILogger<ResourceGroupController> logger)
        {
            _rgService = rgService;
            _logger = logger;
        }


        // POST api/<ResourceGroupController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CreateResourceGroupDto value)
        {
            var rgName = await _rgService.CreateResourceGroupAsync(value);
            return Ok(rgName);
        }
    }    
}
